package util;

public enum WarningStatus {
	CRITICAL, SERIOUSNESS, GENERAL, NO_WARNING;
}
