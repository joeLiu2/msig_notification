package util;

public class OpenDataCoef {
	public static final double A1_LON_LAT_BUFFER = 0.01;
	public static final double A2_LON_LAT_BUFFER = 0.01;
	public static final double CHILD_SAFETY_LON_LAT_BUFFER = 0.01;
	public static final double INSTANCE_TRAFFIC_LON_LAT_BUFFER = 0.01;
	public static final double RAINFALL_LON_LAT_BUFFER = 0.05;
	public static final double SPEED_LIMIT_LON_LAT_BUFFER = 0.0036;
	
	public static final double A1_DIR_BUFFER = 0.6;
	public static final double A2_DIR_BUFFER = 0.6;
	public static final double CHILD_SAFETY_DIR_BUFFER = 0.7;
	public static final double INSTANCE_TRAFFIC_DIR_BUFFER = 0.6;
	public static final double SPEED_LIMIT_DIR_BUFFER = 0.3;
	public static final double RAINFALL_DIR_BUFFER = Math.PI;
	
	public static final String ROAD_TYPE_CONSTRUCTION = "道路施工";
	public static final String ROAD_TYPE_TRAFFIC_JAM = "阻塞";
	public static final String ROAD_TYPE_TRAFFIC_ACCIDENT_1 = "交通障礙";
	public static final String ROAD_TYPE_TRAFFIC_ACCIDENT_2 = "交通事故";
	public static final String ROAD_TYPE_TRAFFIC_CONTROL = "交通管制";
}
