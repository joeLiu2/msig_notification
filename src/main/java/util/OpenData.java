package util;

public enum OpenData {
	A1, A2, CHILD_SAFETY, TRAFFIC_INFO, RAINFALL, SPEED_CAMERA
}
