package util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import org.apache.commons.lang3.StringUtils;

public class MessageUtils {
	public static final String A1_DEFAULT_PUSH_TITLE = "【易肇事路段警示】";
	public static final String A2_DEFAULT_PUSH_TITLE = "【易肇事路段警示】";
	public static final String CHILD_SAFETY_DEFAULT_PUSH_TITLE = "【婦幼安全警示】";
	public static final String INSTANCE_TRAFFIC_DEFAULT_PUSH_TITLE = "【即時路況警示】";
	public static final String RAINFALL_DEFAULT_PUSH_TITLE = "【雨量過量警示】";
	public static final String SPEED_LIMIT_DEFAULT_PUSH_TITLE = "【測速執法警示】";
	public static final String DEFAULT_PUSH_TITLE = "【安全警示】";
	private static final String RAIN_TYPE_RAINING = "雨";
	private static final String DEFAULT_MSG = "KARDI關心您";
	
	private static final String ROAD_CONSTRUCTION = "道路施工";
	private static final String CAR_ACCIDENT = "事故";
	private static final String TRAFFIC_JAM = "阻塞";
	private static final String TRAFFIC_CONTROL = "交通管制";
	
	public static String createWarningMsg(OpenData openData, ResultSet rs) throws SQLException {
		switch(openData) {
			case A1:
				return "前方為易肇事路段，請小心駕駛。";
			case A2:
				return "前方為易肇事路段，請小心駕駛。";
			case CHILD_SAFETY:
				return "前方為婦幼危險路段，請您小心; 如有發現任何異狀請通知";
			case TRAFFIC_INFO:
				String roadType = rs.getString("road_type");
				String location = rs.getString("area_nm");
				switch (roadType) {
					case ROAD_CONSTRUCTION:
						return "前方" + location + "在進行道路施工，請您多多留意!";
					case CAR_ACCIDENT:
						return "前方" + location + "發生交通事故，請您多多留意!";
					case TRAFFIC_JAM:
						return "前方" + location + "車多擁擠，請您多多留意!";
					case TRAFFIC_CONTROL:
						return "前方" + location + "進行交通管制，請您多多留意!";
					default:
						return null;
				}

			case RAINFALL:
				String rainType = rs.getString("rain_type");
				if (StringUtils.isBlank(rainType)) {
					return null;
				}
				
				if (RAIN_TYPE_RAINING.equals(rainType)) {
					return "前方正在下雨，請您多多留意";
				}
				
				return "前方有" + rainType + "發生，請您多多留意。";
			case SPEED_CAMERA:
				return "前方有測速照相，速限:"+ rs.getInt("speed_limit") + "，請依速限行駛。";
			default:
				return DEFAULT_MSG;
		}
	}
	
	public static String createWarningMsg(String rainType) throws SQLException {
		if (StringUtils.isBlank(rainType)) {
			return null;
		}
		
		if (RAIN_TYPE_RAINING.equals(rainType)) {
			return "前方正在下雨，請您多多留意";
		}
		
		return "前方有" + rainType + "發生，請您多多留意。";
	}
	
	public static String getOpenDataQuery_(OpenData openData) {
		StringBuilder sb = new StringBuilder();
		
			switch(openData) {
			case A1:
				sb.append("select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where hash = ?;");
				
				return sb.toString();
			case A2:
				sb.append("select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where hash = ?;");

				return sb.toString();
			case CHILD_SAFETY:
				sb.append("select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where hash = ?;");
	
				return sb.toString();
			case TRAFFIC_INFO:
				sb.append("select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where hash = ?;");
	
				return sb.toString();
			case RAINFALL:
				sb.append("select longitude as lon, latitude as lat, rain_type from public.rainfall_obs_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, rain_type from public.rainfall_obs_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, rain_type from public.rainfall_obs_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, rain_type from public.rainfall_obs_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, rain_type from public.rainfall_obs_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, rain_type from public.rainfall_obs_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, rain_type from public.rainfall_obs_processed where hash = ?")
				.append(" union select longitude as lon, latitude as lat, rain_type from public.rainfall_obs_processed where hash = ?");
	
				return sb.toString();
			case SPEED_CAMERA:
				sb.append("select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?")
				.append(" union select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where hash = ?;");
	
				return sb.toString();
			default:
				return "";
		}
	}
	
	
	public static String getOpenDataQuery(OpenData openData) {
		StringBuilder sb = new StringBuilder();
		
			switch(openData) {
			case A1:
				sb.append("select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a1_processed where longitude >= ?")
				.append(" and longitude <= ? and latitude >= ? and latitude <= ?;");
				
				return sb.toString();
			case A2:
				sb.append("select longitude as lon, latitude as lat, happend_loca from public.traffic_accident_a2_processed where longitude >= ?")
				.append(" and longitude <= ? and latitude >= ? and latitude <= ?;");
				
				return sb.toString();
			case CHILD_SAFETY:
				sb.append("select longitude as lon, latitude as lat, address, dept_nm, branch_nm from public.child_safety_location_processed where longitude >= ?")
				.append(" and longitude <= ? and latitude >= ? and latitude <= ?;");
	
				return sb.toString();
			case TRAFFIC_INFO:
				sb.append("select longitude as lon, latitude as lat, road_type, area_nm from public.pbs_rtr_processed where longitude >= ?")
				.append(" and longitude <= ? and latitude >= ? and latitude <= ?;");
	
				return sb.toString();
			case RAINFALL:
				sb.append("select longitude as lon, latitude as lat, rain_type from public.rainfall_obs_processed where longitude >= ?")
				.append(" and longitude <= ? and latitude >= ? and latitude <= ?;");
	
				return sb.toString();
			case SPEED_CAMERA:
				sb.append("select longitude as lon, latitude as lat, address, speed_limit, direct from public.speed_measuring_location where longitude >= ?")
				.append(" and longitude <= ? and latitude >= ? and latitude <= ?;");
	
				return sb.toString();
			default:
				return "";
		}
	}
	
	public static String insertNotificationQuery(OpenData openData) {
		StringBuilder sb = new StringBuilder();
		
		switch (openData) {
			case A1:
				sb.append("insert into public.traffic_accident_a1_notify");
				break;
			case A2:
				sb.append("insert into public.traffic_accident_a2_notify");
				break;
			case CHILD_SAFETY:
				sb.append("insert into public.child_safety_location_notify");
				break;
			case TRAFFIC_INFO:
				sb.append("insert into public.pbs_rtr_notify");
				break;
			case RAINFALL:
				sb.append("insert into public.rainfall_obs_notify");
				break;
			case SPEED_CAMERA:
				sb.append("insert into public.speed_measuring_location_notify");
				break;
			default:
				return "";	
		}

		sb.append(" (noti_id, user_id, push_title, push_contents, insert_datetime, latitude, longitude, gfb_device_token, event_time, event_lat, event_lon)")
		.append(" values (?, ?, ?, ?, now(), ?, ?, ?, ?, ?, ?);");

		return sb.toString();
	}
	
	public static String getDeviceIdQuery() {
		return "select gfb_device_id as fcm_token from tb_gfb_device_id where user_id = ? order by datestamp desc limit 1";
	}
	
	public static String getTitle(OpenData openData) {
		switch(openData) {
			case A1:
				return A1_DEFAULT_PUSH_TITLE;
			case A2:
				return A2_DEFAULT_PUSH_TITLE;
			case CHILD_SAFETY:
				return CHILD_SAFETY_DEFAULT_PUSH_TITLE;
			case TRAFFIC_INFO:
				return INSTANCE_TRAFFIC_DEFAULT_PUSH_TITLE;
			case RAINFALL:
				return RAINFALL_DEFAULT_PUSH_TITLE;
			case SPEED_CAMERA:
				return SPEED_LIMIT_DEFAULT_PUSH_TITLE;
			default:
				return DEFAULT_PUSH_TITLE;
		}
	}

	/**
	 * 產生pushContent key, 用來判斷是否推播10分鐘內
	 * @param userId
	 * @param content
	 * @return
	 */
	public static String getPushContentKey(String userId, String content) {
		return userId + "-" + content.substring(0, Math.min(content.length(), 15));
	}
	
	/**
	 * randomly generate 9 digit id 
	 * @return uniqId
	 */
	public static String getUniqId() {
		
		Random rm = new Random();
		int temp;
		int len = 9;
		char[] pw = new char[len];
		int count = 0;
		for (int i = 0; i < 3; i++) {
			temp = rm.nextInt(26);
			pw[count++] = (char)(temp + 65);
		}
		
		for (int i = 0; i < 3; i++) {
			temp = rm.nextInt(26);
			pw[count++] = (char)(temp + 97);
		}
		
		for (int i = 0; i < 3; i++) {
			temp = rm.nextInt(10);
			pw[count++] = (char)(temp + 48);
		}
		
		for (int j = 0; j < len; j++) {
			int indx = rm.nextInt(len - j) + j;
			swap(pw, j, indx);
		}
		
		return new String(pw);
	}
	private static void swap(char[] chars, int i, int j) {
		char c = chars[i];
		chars[i] = chars[j];
		chars[j] = c;
	}
}
