package util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolUtils {
	/**
	 * creat and set thread pool 
	 */
	private static final int DEFAULT_THREAD_SIZE = 16;

	private ThreadPoolExecutor pool = null;
	private int threadSize;
	
	public static ThreadPoolUtils create(){
		return new ThreadPoolUtils(DEFAULT_THREAD_SIZE);
	}
	
	public static ThreadPoolUtils create(int threadSize){
		return new ThreadPoolUtils(threadSize);
	}
	
	private ThreadPoolUtils(int threadSize){
		pool = new ThreadPoolExecutor(threadSize, threadSize, 60000L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	}
	
	public void setThreadSize(int threadSize) {
		if (threadSize > 0 && pool.getPoolSize() != threadSize) {
			this.threadSize = threadSize;
			if(pool != null) {
				pool.setCorePoolSize(threadSize);
				pool.setMaximumPoolSize(threadSize);
			}
		}
	}
	
	public synchronized void addTask(Runnable task){
		pool.execute(task);
	}
	
	public synchronized void removeTask(Runnable task){
		pool.remove(task);
	}
	
	public void close(){
		pool.shutdown();
	}
	
	public boolean awaitTermination(){
		boolean finish = true;
		try {
			finish = pool.awaitTermination(50, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return finish;
	}
	
	/** 計算等候處理的任務總數<br>
	 * Because the states of tasks and threads may change dynamically 
	 * during computation, the returned value is only an approximation.
	 * */
	public long countWaitingTasks() {
		long completed = pool.getCompletedTaskCount();
		long total = pool.getTaskCount();
		long count = total - completed;
		if(count <= threadSize) {
			return 0;
		} else {
			return count - threadSize;
		}
	}
}
