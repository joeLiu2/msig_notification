package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import Entity.WarningReq;
import ch.hsr.geohash.GeoHash;

public class DBQueryUtils {
	
	/**
	 * 根據 warningReq，從DB取出附近的OpenData資料 (附近13個區域)
	 * @param openData
	 * @param req
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ResultSet getOpenDataInsideRangeQuery(OpenData openData, WarningReq req, Connection con) throws SQLException {
		if (openData == null || req == null || con == null) {
			return null;
		}
		
		PreparedStatement pstmt = con.prepareStatement(MessageUtils.getOpenDataQuery_(openData));
		
		GeoHash[] neighbors;

		if (OpenData.RAINFALL.equals(openData)) {
			neighbors = get8NearestGeoHash(GeoHash.withCharacterPrecision(req.getLat(), req.getLon(), 5), req.getQuadrant());
		} else {
			neighbors = get13NearestGeoHash(req.getHash(), req.getQuadrant());
		}
			
		for (int i = 0; i < neighbors.length; i++) {
			pstmt.setString(1 + i, neighbors[i].toBase32());
		}

		System.out.println("[RDS Selection]: " + pstmt.toString());
		return pstmt.executeQuery();
	}
	
	
	public static ResultSet getOpenDataInsideRangeQuery(OpenData openData, Double lat, Double lon, Connection con) throws SQLException {
		if (openData == null || lat == null || lon == null) {
			return null;
		}
		
		double buffer;
		switch (openData) {
			case A1:
				buffer = OpenDataCoef.A1_LON_LAT_BUFFER;
				break;
			case A2:
				buffer = OpenDataCoef.A2_LON_LAT_BUFFER;
				break;
			case CHILD_SAFETY:
				buffer = OpenDataCoef.CHILD_SAFETY_LON_LAT_BUFFER;
				break;
			case TRAFFIC_INFO:
				buffer = OpenDataCoef.INSTANCE_TRAFFIC_LON_LAT_BUFFER;
				break;
			case RAINFALL:
				buffer = OpenDataCoef.RAINFALL_LON_LAT_BUFFER;
				break;
			case SPEED_CAMERA:
				buffer = OpenDataCoef.SPEED_LIMIT_LON_LAT_BUFFER;
				break;
			default:
				return null;
		}
		
		PreparedStatement pstmt = con.prepareStatement(MessageUtils.getOpenDataQuery(openData));
		pstmt.setDouble(1, lon - buffer);
		pstmt.setDouble(2, lon + buffer);
		pstmt.setDouble(3, lat - buffer);
		pstmt.setDouble(4, lat + buffer);
		
		System.out.println("[RDS Selection]: " + pstmt.toString());
		return pstmt.executeQuery();
	}
	
	/**
	 * 將推播訊息寫入notify table
	 * @param con
	 * @param openData
	 * @param notiId
	 * @param userId
	 * @param pushContents
	 * @param eventLat
	 * @param eventLon
	 * @param token
	 * @param eventTime
	 * @param sourceLat
	 * @param sourceLon
	 * @throws SQLException
	 */
	public static void insertNotification2RDS(Connection con, OpenData openData, String notiId, String userId, String pushContents, 
			double eventLat, double eventLon, String token, Timestamp eventTime, Double sourceLat, Double sourceLon) throws SQLException {

		PreparedStatement pstmt = con.prepareStatement(MessageUtils.insertNotificationQuery(openData));
		pstmt.setString(1, notiId);
		pstmt.setString(2, userId);
		pstmt.setString(3, MessageUtils.getTitle(openData));
		pstmt.setString(4, pushContents);
		pstmt.setDouble(5, sourceLat);
		pstmt.setDouble(6, sourceLon);
		pstmt.setString(7, token);
		pstmt.setTimestamp(8, eventTime);
		pstmt.setDouble(9, eventLat);
		pstmt.setDouble(10, eventLon);
		
		System.out.println("[RDS Insertion]: " + pstmt.toString());
		int count = pstmt.executeUpdate();
		con.commit();
		System.out.println("[Data Inserted]: " + count);
		pstmt.close();
	}
	
	/**
	 * 從DB取出對應於該userId的 FCM token(register_id/device_id)
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ResultSet getDeviceId(Connection con, String userId) throws SQLException {
		PreparedStatement pstmt = con.prepareStatement(MessageUtils.getDeviceIdQuery());
		pstmt.setString(1, userId);
		System.out.println("[RDS Selection]: " + pstmt.toString());
		return pstmt.executeQuery();
	}
	
	public static GeoHash[] get15NearestGeoHash(GeoHash geoHash) {

		if (geoHash == null) {
			return new GeoHash[15];
		}
		
		GeoHash north = geoHash.getNorthernNeighbour();
		GeoHash east = geoHash.getEasternNeighbour();
		GeoHash south = geoHash.getSouthernNeighbour();
		GeoHash west = geoHash.getWesternNeighbour();
		
		GeoHash furthNorth = north.getNorthernNeighbour();
		GeoHash furthSouth = south.getSouthernNeighbour();
		
		return new GeoHash[] {furthNorth.getWesternNeighbour(), furthNorth, furthNorth.getEasternNeighbour(), 
				west.getNorthernNeighbour(), north, east.getNorthernNeighbour(),
				west, geoHash, east,
				west.getSouthernNeighbour(), south, east.getSouthernNeighbour(),
				furthSouth.getWesternNeighbour(), furthSouth, furthSouth.getEasternNeighbour()};		
	}
	
	public static GeoHash[] get13NearestGeoHash(GeoHash geoHash, int quadrant) {

		if (geoHash == null) {
			return new GeoHash[13];
		}
		
		GeoHash north = geoHash.getNorthernNeighbour();
		GeoHash east = geoHash.getEasternNeighbour();
		GeoHash south = geoHash.getSouthernNeighbour();
		GeoHash west = geoHash.getWesternNeighbour();
		
		GeoHash furthNorth = north.getNorthernNeighbour();
		GeoHash furthSouth = south.getSouthernNeighbour();
		
		if (quadrant == 1) {
			return new GeoHash[] {furthNorth.getWesternNeighbour(), furthNorth, furthNorth.getEasternNeighbour(), 
					west.getNorthernNeighbour(), north, east.getNorthernNeighbour(),
					west, geoHash, east,
					south, east.getSouthernNeighbour(),
					furthSouth, furthSouth.getEasternNeighbour()};
		} else if (quadrant == 2) {
			return new GeoHash[] {furthNorth.getWesternNeighbour(), furthNorth, furthNorth.getEasternNeighbour(), 
					west.getNorthernNeighbour(), north, east.getNorthernNeighbour(),
					west, geoHash, east,
					west.getSouthernNeighbour(), south, 
					furthSouth.getWesternNeighbour(), furthSouth};
		} else if (quadrant == 3) {
			return new GeoHash[] {furthNorth.getWesternNeighbour(), furthNorth,  
					west.getNorthernNeighbour(), north, 
					west, geoHash, east,
					west.getSouthernNeighbour(), south, east.getSouthernNeighbour(),
					furthSouth.getWesternNeighbour(), furthSouth, furthSouth.getEasternNeighbour()};
		} else if (quadrant == 4) {
			return new GeoHash[] {furthNorth, furthNorth.getEasternNeighbour(), 
					north, east.getNorthernNeighbour(),
					west, geoHash, east,
					west.getSouthernNeighbour(), south, east.getSouthernNeighbour(),
					furthSouth.getWesternNeighbour(), furthSouth, furthSouth.getEasternNeighbour()};
		}
		
		return new GeoHash[13];
	}
	
	public static GeoHash[] get8NearestGeoHash(GeoHash geoHash, int quadrant) {

		if (geoHash == null) {
			return new GeoHash[8];
		}
		
		GeoHash north = geoHash.getNorthernNeighbour();
		GeoHash east = geoHash.getEasternNeighbour();
		GeoHash south = geoHash.getSouthernNeighbour();
		GeoHash west = geoHash.getWesternNeighbour();
			
		if (quadrant == 1) {
			return new GeoHash[] {north.getWesternNeighbour(), north, north.getEasternNeighbour(),
					west, geoHash, east,
					south, south.getEasternNeighbour()};
		} else if (quadrant == 2) {
			return new GeoHash[] {north.getWesternNeighbour(), north, north.getEasternNeighbour(),
					west, geoHash, east,
					south.getWesternNeighbour(), south};
		} else if (quadrant == 3) {
			return new GeoHash[] {north.getWesternNeighbour(), north, 
					west, geoHash, east,
					south.getWesternNeighbour(), south, south.getEasternNeighbour()};
		} else if (quadrant == 4) {
			return new GeoHash[] {north, north.getEasternNeighbour(),
					west, geoHash, east,
					south.getWesternNeighbour(), south, south.getEasternNeighbour()};
		}
		
		return new GeoHash[8];
	}
}
