package util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Entity.WarningReq;

public class FormulaUtils {
	
	private static final int R = 6371000; // the radius of earth (unit: m)
	private final static Logger LOG = LoggerFactory.getLogger(FormulaUtils.class);
	
	private static final int MIN_ALERT_TIME = 3;
	private static final int ALERT_TIME = 30;
	private static final int BUFFER = 180;
	private static final int MAX_DIST_ALERT = 300;
	private static final double GENERAL_WARNING_DIST = 1000d;
	private static final String SEPERATOR_1 = ";";
	private static final String SEPERATOR_2 = ",";

	/**
	 * convert degree to radian
	 * @param degree
	 * @return the radian
	 */
	public static double deg2Rad(double degree) {
		return degree * Math.PI / 180.0;
	}
	
	/**
	 * Giving starting point (latitude, longitude) and ending point (latitude, longitude)
	 * Return the absolute distance between starting and ending point
	 * @param  lat1 the latitude of starting point
	 * @param  lon1 the longitude of starting point
	 * @param  lat2 the latitude of ending point
	 * @param  lon2 the longitude of ending point
	 * @return the distance between starting and ending point (unit : m)   
	 */
	public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
		double rlat1 = deg2Rad(lat1);
		double rlon1 = deg2Rad(lon1);
		double rlat2 = deg2Rad(lat2);
		double rlon2 = deg2Rad(lon2);
		
		double d = Math.sin(rlat1) * Math.sin(rlat2) + 
				Math.cos(rlat1) * Math.cos(rlat2) * Math.cos(rlon2 - rlon1);
		
		double dist = Math.acos(d) * R;
		LOG.info("distance between two point: {}", dist);
		
		return dist;
	}
	
	/**
	 * evaluate warning status
	 * @param bearing
	 * @param fromLat
	 * @param fromLon
	 * @param toLat
	 * @param toLon
	 * @param speed
	 * @param openData
	 * @return
	 */
	public static WarningStatus evaluateWarningStatus(Double bearing, Double fromLat, Double fromLon, Double toLat, Double toLon, Double speed, OpenData openData) {
		if (bearing == null || fromLat == null || fromLon == null || toLat == null || toLon == null || openData == null) {
			System.out.println("Infomation is not complete when evaluating warningStatus");
			return WarningStatus.NO_WARNING;
		}
		
		Double sourceBearing = getBearing(fromLat, fromLon, toLat, toLon);
		System.out.println("[bearing]: " + bearing + "  [sourceBearing]: " + sourceBearing);
		
		double diff = getBearingDiff(bearing, sourceBearing);
		System.out.println("[bearingDiff]: " + diff);
		Double criDist, ignDist, warnDist, dirBuff;
		
		dirBuff = getBearningWarningRange(speed, openData);
		System.out.println("[bearingRange]: " + dirBuff);

		double[] warningRange = getBetweenDistanceWarningRange(speed, openData);

		ignDist = warningRange[0];
		criDist = warningRange[1];
		
		System.out.println("[distanceRange]: " + ignDist + " ~ " + criDist);
		Double distance = getDistance(fromLat, fromLon, toLat, toLon);
		System.out.println("[distance]: " + distance);
		warnDist = GENERAL_WARNING_DIST;
		
		// out of sight
		if (diff > dirBuff) {
			return WarningStatus.NO_WARNING;
		}
		
		if (criDist > distance && distance > ignDist) {
			return WarningStatus.CRITICAL;
		} else if (warnDist > distance && distance > ignDist) {
			return WarningStatus.GENERAL;
		}
	
		// not within certain range
		return WarningStatus.NO_WARNING;
	}
	
	/**
	 * evaluate warning status from warningReq req
	 * @param req
	 * @param toLat
	 * @param toLon
	 * @return
	 */
	public static WarningStatus evaluateWarningStatus(WarningReq req, Double toLat, Double toLon) {
		if (req.getBearing() == null || req.getLat() == null || req.getLon() == null || req.getBearingRange() == null || req.getDistRange() == null || toLat == null || toLon == null) {
			System.out.println("infomation is not complete when evaluating warningStatus");
			return WarningStatus.NO_WARNING;
		}
		
		Double fromLat = req.getLat();
		Double fromLon = req.getLon();
		Double bearing = req.getBearing();
		
		Double sourceBearing = getBearing(fromLat, fromLon, toLat, toLon);
		
		System.out.println("[bearing]: " + bearing + "  [sourceBearing]: " + sourceBearing);
		
		double diff = getBearingDiff(bearing, sourceBearing);
		System.out.println("[bearingDiff]: " + diff);
		Double criDist, ignDist, warnDist, dirBuff;
		dirBuff = req.getBearingRange();
		
		System.out.println("[bearingRange]: " + dirBuff);
		double[] warningRange = req.getDistRange();
		ignDist = warningRange[0];
		criDist = warningRange[1];
		
		System.out.println("[distanceRange]: " + ignDist + "~" + criDist);
		warnDist = GENERAL_WARNING_DIST;
		Double distance = getDistance(fromLat, fromLon, toLat, toLon);
		System.out.println("[distance]: " + distance);
		
		// out of sight
		if (diff > dirBuff) {
			return WarningStatus.NO_WARNING;
		}

		if (criDist > distance && distance > ignDist) {
			return WarningStatus.CRITICAL;
		} else if (warnDist > distance && distance > ignDist) {
			return WarningStatus.GENERAL;
		}
	
		// not within certain range
		return WarningStatus.NO_WARNING;
	}
	
	public static double[] getBetweenDistanceWarningRange(Double speed, OpenData openData) {
		if (speed == null || speed == 0) {
			return new double[] {0, 200};
		}
		
		return new double[] {MIN_ALERT_TIME * speed, Math.max(MAX_DIST_ALERT, ALERT_TIME * speed + BUFFER)};
	}
	
	public static double getBearningWarningRange(Double speed, OpenData openData) {
		if (speed == null || speed == 0) {
			return Math.PI;
		}
		
		return Math.atan(12.0 / speed);
	}
	
	public static double getBearingDiff(Double bearing1, Double bearing2) {
		bearing1 = (bearing1 + 2 * Math.PI) % (2 * Math.PI);
		bearing2 = (bearing2 + 2 * Math.PI) % (2 * Math.PI);
		
		double diff = Math.min(Math.abs(bearing1 - bearing2), Math.abs(bearing2 - bearing1));
		
		return Math.min(diff, Math.abs(2 * Math.PI - diff));
	}
	
	/**
	 * getting bearing angle
	 * @param fromLat
	 * @param fromLon
	 * @param toLat
	 * @param toLon
	 * @return
	 */
	public static double getBearing(Double fromLat, Double fromLon, Double toLat, Double toLon) {
		double dealL = toLon - fromLon;
		toLat = deg2Rad(toLat);
		fromLat = deg2Rad(fromLat);
		double x = Math.cos(toLat) * Math.sin(dealL);
		double y = Math.cos(fromLat) * Math.sin(toLat) - Math.sin(fromLat) * Math.cos(toLat) * Math.cos(dealL);
		
		return Math.atan2(x, y);
	}
	
	public static boolean ensureNotShortestDistance(double lat1, double lon1, double lat2, double lon2, double distance) {
		double rlat1 = deg2Rad(lat1);
		double rlon1 = deg2Rad(lon1);
		double rlat2 = deg2Rad(lat2);
		double rlon2 = deg2Rad(lon2);
		
		return Math.abs(rlat1 - rlat2) * R > distance || Math.abs(rlon1 - rlon2) * R > distance;
	}
	
	/**
	 * 從連續15秒的經緯度位置 預估最後幾秒的前進方向
	 * @param location
	 * @return
	 */
	public static Double getBearingFromLocationStore(String location) {
		
		String[] locations = location.split(SEPERATOR_1);
		if (locations != null && locations.length > 5) {
			String endloc = locations[locations.length - 1];
			String[] ed = endloc.split(",");
			Double toLat = Double.valueOf(ed[1]);
			Double toLon = Double.valueOf(ed[0]);

			for (int i = locations.length - 3; i > locations.length - 6; i--) {
				String startLoc = locations[i];
				String[] st = startLoc.split(SEPERATOR_2);
				
				Double fromLat = Double.valueOf(st[1]);
				Double fromLon = Double.valueOf(st[0]);
				
				if (fromLat == toLat && fromLon == toLon) {
					continue;
				}
				
				return getBearing(fromLat, fromLon, toLat, toLon);
			}
		}
		return 0d;
	}
	
	/**
	 * 從連續15秒的速度，取最後幾秒的速度平均值
	 * @param speedStore
	 * @return
	 */
	public static double getLastSpeed(String speedStore) {
		
		String[] speeds = speedStore.split(SEPERATOR_1);
		if (speeds != null && speeds.length > 3) {
			Double last3Speeds = 0d;
			for (int i = speeds.length - 1; i  > speeds.length - 4; i--) {
				last3Speeds += Double.valueOf(speeds[i]);
			}
			
			return last3Speeds / 3;
		}
		return 0d;
	}
}
