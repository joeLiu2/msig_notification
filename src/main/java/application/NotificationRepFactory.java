package application;

import java.sql.Connection;

import com.amazonaws.services.dynamodbv2.document.Table;
import util.OpenData;

import repository.NotificationWarningRep;

public class NotificationRepFactory {
	public static NotificationWarningRep getNotificationWarning(OpenData openData, Connection con, Table table) {
		if (OpenData.A1.equals(openData)) {
			return new TrafficAccidentA1Notification(con, table);
		} else if (OpenData.A2.equals(openData)) {
			return new TrafficAccidentA2Notification(con, table);
		} else if (OpenData.TRAFFIC_INFO.equals(openData)) {
			return new InstantTrafficNotification(con, table);
		} else if (OpenData.SPEED_CAMERA.equals(openData)) {
			return new SpeedMeasuringNotification(con, table);
		}
		
		return null;
	}
}
