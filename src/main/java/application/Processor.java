package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import org.apache.commons.lang3.exception.ExceptionUtils;
import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import util.DBQueryUtils;
import util.FormulaUtils;
import util.MessageUtils;
import util.OpenData;
import util.OpenDataCoef;
import util.WarningStatus;

import Entity.WarningReq;
import ch.hsr.geohash.GeoHash;

public class Processor implements Runnable {
	private String[] element;
	private Connection con;
	private Table table;
	private final static Logger LOG = LoggerFactory.getLogger(Processor.class);
	private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	AmazonS3 client = new AmazonS3Client();
	
	private static final String USER_ID 			= "user_id";
	private static final String RAIN_TYPE 			= "rain_type";
	private static final String ROAD_TYPE 			= "road_type";
	private static final String LON 				= "lon";
	private static final String LAT 				= "lat";
	private static final String NOTI_ID 			= "noti_id";
	private static final String DIRECT				= "direct";
    private static Map<String, String> tokenMap 	= new HashMap<>();
    
    
    private static final int NO_USER_ID 	= 0;
    private static final int NO_EVENT_LON 	= 5;
    private static final int NO_EVENT_LAT 	= 6;
    private static final int NO_EVENT_TIME 	= 3;
    private static final int NO_LOCATION   	= 17;
    private static final int NO_SPEED		= 19;
    
    private static final int TEN_MIN_IN_MILLI = 10 * 60 * 1000;
    
    private static final double KILM_HH_M_SS_CONVERT_COEF = 1000d / 3600;
    
    private static final String NORTH = "N";
    private static final String EAST = "E";
    private static final String SOUTH = "S";
    private static final String WEST = "W";
    private static final String NORTH_SOUTH = "NS";
    private static final String WEST_EAST = "WE";
    
    private static final Set<String> DIRECTION_SET = new HashSet<>(Arrays.asList(NORTH, EAST, SOUTH, WEST, NORTH_SOUTH, WEST_EAST));
	
    public Processor(String[] element, Connection con, Table table) {
		this.element = element;
		this.con = con;
		this.table = table;
	}
	
	@Override
	public void run() {	
		
		try {
			WarningReq req = getWarningReq(element);
			if (req == null || req.getSpeed() == null || req.getSpeed() == 0) {
				LOG.info("[WarningReq]: request is null or speed is 0 or empty");
				return;
			}
			
			if (req.getBearing() == null) {
				LOG.info("[WarningReq]: bearing is null");
				return;
			}
			
			for (OpenData openData : OpenData.values()) {
				if (OpenData.RAINFALL.equals(openData)) {
					LOG.info("openData type: rainfall warning");
					continue;
				}
				
				if (OpenData.CHILD_SAFETY.equals(openData)) {
					LOG.info("openData type: child safety warning");
					continue;
				}
				checkWarning(openData, req);
			}
			
		} catch (Exception e) {
			LOG.error(ExceptionUtils.getStackTrace(e));
		} 
	}
	
	/**
	 * 從rawData(WarningReq)得到的資料如經緯度和速度等比對openData資料，查看是否有達到推播的條件(warning)
	 * @param openData
	 * @param req
	 */
	private void checkWarning(OpenData openData, WarningReq req) {
		// checking request 
		if (req == null || req.getUserId() == null || req.getLat() == null || req.getLon() == null || req.getLat() == 0 || req.getLon() == 0 || req.getHash() == null) {
			LOG.warn("[WarningReq]: Invalid request");
			return;
		}
		
		ResultSet rs = null;
		Double lat, lon;	// OpenData 的經緯度
		
		// 取得該經緯度附近OpenData的資料
		try {
			LOG.info("[WarningReq]: Getting OpenData....");
			rs = DBQueryUtils.getOpenDataInsideRangeQuery(openData, req, con);
		} catch (SQLException e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
			return;
		}
		
		if (rs == null) {
			LOG.warn("[WarningReq]: ResultSet from openData is null");
			return;
		}
		
		try {
			if (OpenData.SPEED_CAMERA.equals(openData)) {

				while (rs.next()) {
					
					String dir = rs.getString(DIRECT);	
					// 目前以車子方向分成4個象限，判斷測速照向方向是否和車輛行徑方向大約一致
					if (DIRECTION_SET.contains(dir) && !isAlignDirection(req.getQuadrant(), dir)) {
						LOG.info("[OpenData]: Speed_measuring is not in your direction");
						continue;
					}
							
					lon = rs.getDouble(LON);
					lat = rs.getDouble(LAT);
					
					LOG.info("[OpenData]: openData src latlng1 : (" + lat + ", " + lon + ")");
					
					WarningStatus warningStatus = FormulaUtils.evaluateWarningStatus(req, lat, lon);
				
					LOG.info("[WarningStatus]: {}", warningStatus);
					
					// 超過範圍不推播
					if (WarningStatus.NO_WARNING.equals(warningStatus)) {
						LOG.info("[OpenData]: According to this openData set: It's not in the critical range");
						continue;
					} 
				
					// 推到推播條件，將資料計錄到DB
					if (WarningStatus.CRITICAL.equals(warningStatus)) {
						preparePushNotification(req.getUserId(), req.getLat(), req.getLon(), openData, rs, lat, lon, req.getEventTime(), null);
						break;
					}
			    }
			} else if (OpenData.RAINFALL.equals(openData)) {
				
				Double minRainFallDist = Double.MAX_VALUE;
				String rainType = null;
				double sourceLat = -1d, sourceLon = -1d;

				while (rs.next()) {
					lon = rs.getDouble(LON);
					lat = rs.getDouble(LAT);
					LOG.info("[OpenData]: openData src latlng1 : (" + lat + ", " + lon + ")");
					
				    if (FormulaUtils.ensureNotShortestDistance(lat, lon, req.getLat(), req.getLon(), minRainFallDist)) {
				    	continue;
				    }
					
					Double dist = FormulaUtils.getDistance(lat, lon, req.getLat(), req.getLon());
					if (dist < minRainFallDist) {
						minRainFallDist = dist;
						rainType = rs.getString(RAIN_TYPE);
						sourceLat = lat;
						sourceLon = lon;
					}
			    }
				
				// 如找尋到最近的雨量觀測站資料，將需要推播的訊息計錄到 DB
				if (minRainFallDist < Double.MAX_VALUE && !StringUtils.isBlank(rainType) && sourceLat != -1d && sourceLon != -1d) {
					preparePushNotification(req.getUserId(), req.getLat(), req.getLon(), openData, rs, sourceLat, sourceLon, req.getEventTime(), rainType);
				}
			
			} else {
				while (rs.next()) {
					lon = rs.getDouble(LON);
					lat = rs.getDouble(LAT);
					LOG.info("[OpenData]: openData src latlng1 : (" + lat + ", " + lon + ")");
					
					WarningStatus warningStatus = FormulaUtils.evaluateWarningStatus(req, lat, lon);
				
					LOG.info("[WarningStatus]: {}", warningStatus);
					
					// 超過範圍不推播
					if (WarningStatus.NO_WARNING.equals(warningStatus)) {
						LOG.info("[OpenData]: According to this openData set: It's not in the critical range");
						continue;
					} 
				
					// 推到推播條件，將資料計錄到DB
					if (WarningStatus.CRITICAL.equals(warningStatus)) {
						preparePushNotification(req.getUserId(), req.getLat(), req.getLon(), openData, rs, lat, lon, req.getEventTime(), null);
						break;
					}
			    }
			}
		} catch (Exception e) {
			LOG.error(ExceptionUtils.getStackTrace(e));
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				LOG.error(ExceptionUtils.getStackTrace(e));
			} catch (Exception e) {
				LOG.error(ExceptionUtils.getStackTrace(e));
			}
		}
	}
	
	/**
	 * 查看角度是和direction描述大致穩和，方向區分為 N, E, S, W
	 * @param bearing
	 * @param dir
	 * @return
	 */
	private boolean isAlignDirection(int quadrant, String dir) {
		
		if (NORTH_SOUTH.equals(dir) || WEST_EAST.equals(dir)) {
			return true;
		}
		
		if (quadrant == 1) {
			return NORTH.equals(dir) || EAST.equals(dir);
		// 東南向
		} else if (quadrant == 4) {
			return SOUTH.equals(dir) || EAST.equals(dir);
		// 西北向
		} else if (quadrant == 2) {
			return NORTH.equals(dir) || WEST.equals(dir);
		// 西南向
		} else if (quadrant == 3) {
			return SOUTH.equals(dir) || WEST.equals(dir);
		} 
		
		return false;
	}
	
	/**
	 * get WarningMsg according to OpenData type and verify if the same message is inserted to DB within 10 mins, 
	 * insert push notification to DB
	 * @param userId
	 * @param eventLat
	 * @param eventLon
	 * @param openData
	 * @param rs
	 * @param sourceLat
	 * @param sourceLon
	 * @param sound
	 * @param eventTime
	 * @param rainType
	 */
	private void preparePushNotification(String userId, double eventLat, double eventLon, OpenData openData, ResultSet rs, Double sourceLat, Double sourceLon, Timestamp eventTime, String rainType) {
		String warningMsg = null;
		
		try {
	    	warningMsg = MessageUtils.createWarningMsg(openData, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	    
	    if (StringUtils.isBlank(warningMsg)) {
	    	System.out.println("No warningMessage found or no warning at all");
	    	return;
	    }
	    
	    if (isHappenWithInTime(userId, warningMsg)) {
	    	LOG.info("UserID:" + userId + " checking the pushNotification - Within 10 min");
	    	return;
	    }
	    
	    insertNotificationToDB(userId, eventLat, eventLon, openData, sourceLat, sourceLon, eventTime, warningMsg);
	}
	
	/**
	 * insert notification to table
	 * @param userId
	 * @param eventLat
	 * @param eventLon
	 * @param openData
	 * @param rs
	 * @param sourceLat
	 * @param sourceLon
	 * @param sound
	 * @param eventTime
	 * @param rainType
	 */
	private void insertNotificationToDB(String userId, double eventLat, double eventLon, OpenData openData, Double sourceLat, Double sourceLon, Timestamp eventTime, String warningMsg) {
	    String token = "";
	    String notiId = MessageUtils.getUniqId();
	    
	    if (!tokenMap.containsKey(userId)) {
    		token = getFcmToken(userId);
    		tokenMap.put(userId, token);
    	}
    		
    	token = tokenMap.get(userId);
    		
    	if (!StringUtils.isBlank(token)) {
    		
    		LOG.info("Inserting nortification record to RDS...");
    		try {
    	    	DBQueryUtils.insertNotification2RDS(con, openData, notiId, userId, warningMsg, eventLat, eventLon, token, eventTime, sourceLat, sourceLon);
    	    } catch (Exception e) {
    	    	LOG.info("Exception:{}", e.getMessage());
    	    	LOG.error(ExceptionUtils.getStackTrace(e));
    	    }
    		
    		LOG.info("Inserting nortification record to DynamoDB...");
    		try {
    			insertNotification2DynamoDB(userId, MessageUtils.getTitle(openData), notiId, warningMsg, token, openData);
    		} catch (Exception e) {
    			LOG.info("Exception:{}", e.getMessage());
    			LOG.error(ExceptionUtils.getStackTrace(e));
    		}
    	} else {
    		LOG.warn("token is blank");
    	}	    
	}
	
	/**
	 * 將進料insert進DynamoDB, 做後續推播
	 * @param userId
	 * @param title
	 * @param notiId
	 * @param currTime
	 * @param warningMsg
	 * @param token
	 * @param sound
	 * @throws Exception
	 */
	private void insertNotification2DynamoDB(String userId, String title, String notiId, String warningMsg, String token, OpenData openData) throws Exception {
		// corner case check
		if (userId == null || notiId == null || warningMsg == null || token == null) {
			LOG.info("userId: {}", userId);
			LOG.info("notiId: {}", notiId);
			LOG.info("warningMsg: {}", warningMsg);
			LOG.info("token: {}", token);
			
			LOG.warn("push infomation is not complete");
			return;
		}
		
//		setPusContentTimer(userId, warningMsg, 60 * 5);
		
		long currTimeStamp = System.currentTimeMillis();
		// insert notification to dynamoDB
		PutItemOutcome outcome = table
        		.putItem(new Item().withPrimaryKey(USER_ID, userId, NOTI_ID, notiId)
        		.withLong("time_stamp", currTimeStamp)
        		.withString("content", warningMsg)
        		.withString("title", title)
        		.withString("type", openData.toString())
        		.withString("token", token));

	    LOG.info(outcome.toString());
	}
	
	/**
	 * 從S3 bucket資料，取得OpenData 推播需要的資訊
	 * @param element
	 * @return
	 * @throws Exception
	 */
	public WarningReq getWarningReq(String[] element) throws Exception {
		
		if (element == null || element.length < 53) {
			throw new NullPointerException("[WarningReq]: element is not complete");
		}
		
		LOG.info("[WarningReq]: getting warningReq ....");
		
		WarningReq req = new WarningReq();

		Double lon = Double.valueOf(element[NO_EVENT_LON]);
		Double lat = Double.valueOf(element[NO_EVENT_LAT]);
		Double bearing = FormulaUtils.getBearingFromLocationStore(element[NO_LOCATION]);
		int quadrant = getQuadrant(bearing);
		Double speed = FormulaUtils.getLastSpeed(element[NO_SPEED]) * KILM_HH_M_SS_CONVERT_COEF;
		
		req.setUserId(element[NO_USER_ID]);
		req.setLon(lon);
		req.setLat(lat);
		req.setHash(GeoHash.withCharacterPrecision(lat, lon, 6));
		req.setSpeed(speed);
		req.setBearing(bearing);
		req.setQuadrant(quadrant);
		req.setEventTime(convertStrToTimestamp(element[NO_EVENT_TIME]));
		
		//**** 從速度來推估推播範圍   ****//
		req.setBearingRange(FormulaUtils.getBearningWarningRange(speed, null));
		req.setDistRange(FormulaUtils.getBetweenDistanceWarningRange(speed, null));

		return req;
	}
	
	/**
	 * 從S3 bucket 讀取到的時間字串，轉成Timestamp
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public Timestamp convertStrToTimestamp(String date) throws ParseException {
		if (StringUtils.isBlank(date)) {
			throw new IllegalArgumentException("date cannot be blank");
		}
		
		date = date.substring(0, Math.min(19, date.length()));
		
		Date parsedTimeStamp = format.parse(date);

	    return new Timestamp(parsedTimeStamp.getTime());
	}
	
//	private void setPusContentTimer(String userId, String pushContents, long sec) {
//		System.out.println("setting timer .... ");
//		RedisClient redisClient = RedisClient.create("redis://d9KSdCdxoFdEVQZkJ0k9h3xEW4Lc1n7q@redis-18423.c54.ap-northeast-1-2.ec2.cloud.redislabs.com:18423");
//		StatefulRedisConnection<String, String> connection = redisClient.connect();
//		RedisCommands<String, String> jedis = connection.sync();
//		
//		String key = MessageUtils.getPushContentKey(userId, pushContents);
//		jedis.set(key, "0");
//		jedis.expire(key, sec);
//		
//		connection.close();
//		redisClient.shutdown();
//	}
	
	/**
	 * 判斷發送時間是否在10分鐘內
	 * @param userId
	 * @param pushContents
	 * @return
	 */
	private boolean isHappenWithInTime(String userId, String pushContents) {
		System.out.println("checking timer......");

//		RedisClient redisClient = RedisClient.create("redis://d9KSdCdxoFdEVQZkJ0k9h3xEW4Lc1n7q@redis-18423.c54.ap-northeast-1-2.ec2.cloud.redislabs.com:18423");
//		StatefulRedisConnection<String, String> connection = redisClient.connect();
//		RedisCommands<String, String> jedis = connection.sync();
//		
//		boolean isWithinTime = true;
//		
//		String key = MessageUtils.getPushContentKey(userId, pushContents);
//		System.out.println("key:" + key);
//		isWithinTime = jedis.exists(key) > 0;
//		
//		connection.close();
//		redisClient.shutdown();
//		
//		return isWithinTime;
		
		long currTimeStamp = System.currentTimeMillis();
		
		
        Map<String, Object> valueMap = new HashMap<>();
        valueMap.put(":id", userId);
        valueMap.put(":content", pushContents);
        valueMap.put(":time", currTimeStamp - TEN_MIN_IN_MILLI); // 確認是否在10分鐘內
        
        QuerySpec querySpec = new QuerySpec().withKeyConditionExpression("user_id = :id")
        		.withFilterExpression("content = :content and time_stamp >= :time")
                .withValueMap(valueMap);
        

        ItemCollection<QueryOutcome> items = null;
        Iterator<Item> iterator = null;

        try {
            items = table.query(querySpec);
            
            if (items == null) {
            	return false;
            }
            
            iterator = items.iterator();
            
            if (iterator != null && iterator.hasNext()) {
            	return true;
            }
            
            return false;
            
        } catch (Exception e) {
        	e.printStackTrace();
        	return false;
        }
	}
	
	private String getFcmToken(String userId) {
		return "123";
	}

	/**
	 * 從bearing，取得方向所在象限
	 * @param bearing
	 * @return
	 */
	private int getQuadrant(Double bearing) {
		if (bearing == null) {
			return -1;
		}
		
		if (bearing > 0 && Math.PI/2 >= bearing) {
			return 1;
		// 東南向
		} else if (bearing > Math.PI/2 && Math.PI > bearing) {
			return 4;
		// 西北向
		} else if (bearing > -Math.PI/2 && 0 >= bearing) {
			return 2;
		// 西南向
		} else if (bearing > -Math.PI && -Math.PI/2 >= bearing) {
			return 3;
		} else if (bearing == -Math.PI || bearing == Math.PI) {
			return 4;
		}
		
		return -1;
	}
}
