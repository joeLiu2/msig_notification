package application;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.document.Table;
import util.FormulaUtils;
import util.OpenData;

import Entity.WarningReq;
import ch.hsr.geohash.GeoHash;
import repository.NotificationWarningRep;

public class MainProcessor implements Runnable {
	private String[] element;
	private Connection con;
	private Table table;
	private final static Logger LOG = LoggerFactory.getLogger(Processor.class);
	
	
//    private Map<String, String> tokenMap 	= new HashMap<>();
    
    private static final int NO_USER_ID 	= 0;
    private static final int NO_EVENT_LON 	= 5;
    private static final int NO_EVENT_LAT 	= 6;
    private static final int NO_EVENT_TIME 	= 3;
    private static final int NO_LOCATION   	= 17;
    private static final int NO_SPEED		= 19;

    private static final double KILM_HH_M_SS_CONVERT_COEF = 1000d / 3600; // 將 單位 (km/hr) 轉成 (m/s) 成的常數

    public MainProcessor(String[] element, Connection con, Table table) {
		this.element = element;
		this.con = con;
		this.table = table;
//		this.tokenMap = tokenMap;
	}
	
	@Override
	public void run() {	
		
		try {
			WarningReq req = getWarningReq(element);
			if (req == null) {
				LOG.info("[WarningReq]: Request is null");
				return;
			}
			
			if (req.getBearing() == null) {
				LOG.info("[WarningReq]: Bearing is null");
				return;
			}
			
			for (OpenData openData : OpenData.values()) {
				if (OpenData.RAINFALL.equals(openData)) {
					LOG.info("OpenData type: Rainfall warning");
					continue;
				}
				
				if (OpenData.CHILD_SAFETY.equals(openData)) {
					LOG.info("OpenData type: Child and Women safety warning");
					continue;
				}
				
				NotificationWarningRep notificationWarning = NotificationRepFactory.getNotificationWarning(openData, con, table);
				notificationWarning.checkWarning(req);
			}
			
		} catch (Exception e) {
			LOG.error(ExceptionUtils.getStackTrace(e));
		} 
	}
	
	/**
	 * 從S3 bucket資料，取得OpenData 推播需要的資訊
	 * @param element
	 * @return
	 * @throws Exception
	 */
	public WarningReq getWarningReq(String[] element) throws Exception {
		
		if (element == null || element.length < 53) {
			throw new NullPointerException("[WarningReq]: Element is not complete");
		}
		
		LOG.info("[WarningReq]: Getting warningReq ....");
		
		WarningReq req = new WarningReq();

		Double lon = Double.valueOf(element[NO_EVENT_LON]);
		Double lat = Double.valueOf(element[NO_EVENT_LAT]);
		Double bearing = FormulaUtils.getBearingFromLocationStore(element[NO_LOCATION]);
		int quadrant = getQuadrant(bearing);
		Double speed = FormulaUtils.getLastSpeed(element[NO_SPEED]) * KILM_HH_M_SS_CONVERT_COEF;
		
		req.setUserId(element[NO_USER_ID]);
		req.setLon(lon);
		req.setLat(lat);
		req.setHash(GeoHash.withCharacterPrecision(lat, lon, 6));
		req.setSpeed(speed);
		req.setBearing(bearing);
		req.setQuadrant(quadrant);
		req.setEventTime(convertStrToTimestamp(element[NO_EVENT_TIME]));
		
		//**** 從速度來推估推播範圍   ****//
		req.setBearingRange(FormulaUtils.getBearningWarningRange(speed, null));
		req.setDistRange(FormulaUtils.getBetweenDistanceWarningRange(speed, null));

		return req;
	}
	
	/**
	 * 從S3 bucket 讀取到的時間字串，轉成Timestamp
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public Timestamp convertStrToTimestamp(String date) throws ParseException {
		if (StringUtils.isBlank(date)) {
			throw new IllegalArgumentException("Date cannot be blank");
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		date = date.substring(0, Math.min(19, date.length()));
		
		Date parsedTimeStamp = format.parse(date);

	    return new Timestamp(parsedTimeStamp.getTime());
	}

	/**
	 * 從bearing，取得方向所在象限
	 * @param bearing
	 * @return
	 */
	private int getQuadrant(Double bearing) {
		if (bearing == null) {
			return -1;
		}
		
		if (bearing > 0 && Math.PI/2 >= bearing) {
			return 1;
		// 東南向
		} else if (bearing > Math.PI/2 && Math.PI > bearing) {
			return 4;
		// 西北向
		} else if (bearing > -Math.PI/2 && 0 >= bearing) {
			return 2;
		// 西南向
		} else if (bearing > -Math.PI && -Math.PI/2 >= bearing) {
			return 3;
		} else if (bearing == -Math.PI || bearing == Math.PI) {
			return 4;
		}
		
		return -1;
	}
}
