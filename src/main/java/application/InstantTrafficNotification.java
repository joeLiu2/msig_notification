package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;


import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.document.Table;
import util.DBQueryUtils;
import util.FormulaUtils;
import util.OpenData;
import util.WarningStatus;

import Entity.WarningReq;
import repository.NotificationWarningRep;

public class InstantTrafficNotification extends NotificationWarningRep {
	private Connection con;
	private static final String LON 				= "lon";
	private static final String LAT 				= "lat";
	
	private final static Logger LOG = LoggerFactory.getLogger(InstantTrafficNotification.class);
	
	public InstantTrafficNotification(Connection con, Table table) {
		super(con, table);
		this.con = con;
	}
	
	/**
	 * 從rawData(WarningReq)得到的資料如經緯度和速度等比對openData資料，查看是否有達到推播的條件(warning)
	 * @param openData
	 * @param req
	 */
	public void checkWarning(WarningReq req) {
		// checking request 
		if (req == null || req.getUserId() == null || req.getLat() == null || req.getLon() == null || req.getLat() == 0 || req.getLon() == 0 || req.getHash() == null) {
			LOG.warn("[WarningReq]: Invalid request");
			return;
		}
		
		ResultSet rs = null;
		Double lat, lon;	// OpenData 的經緯度
		
		// 取得該經緯度附近OpenData的資料
		try {
			LOG.info("[WarningReq]: Getting OpenData....");
			rs = DBQueryUtils.getOpenDataInsideRangeQuery(OpenData.TRAFFIC_INFO, req, con);
		} catch (SQLException e) {
			LOG.error(e.getMessage());
			LOG.error(ExceptionUtils.getStackTrace(e));
			return;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			LOG.info("Getting Data Error: " + e.getMessage());
		}
		
		if (rs == null) {
			LOG.warn("[WarningReq]: ResultSet from openData is null");
			return;
		}
		
		try {
			while (rs.next()) {
				lon = rs.getDouble(LON);
				lat = rs.getDouble(LAT);
				LOG.info("[OpenData]: OpenData src latlng1 : (" + lat + ", " + lon + ")");
				
				WarningStatus warningStatus = FormulaUtils.evaluateWarningStatus(req, lat, lon);
			
				LOG.info("[WarningStatus]: {}", warningStatus);
				
				// 超過範圍不推播
				if (WarningStatus.NO_WARNING.equals(warningStatus)) {
					LOG.info("[OpenData]: According to this openData set: It's not in the critical range");
					continue;
				} 
			
				// 推到推播條件，將資料計錄到DB
				if (WarningStatus.CRITICAL.equals(warningStatus)) {
					preparePushNotification(req.getUserId(), req.getLat(), req.getLon(), OpenData.TRAFFIC_INFO, rs, lat, lon, req.getEventTime());
					break;
				}
		    }
			
		} catch (Exception e) {
			LOG.error(ExceptionUtils.getStackTrace(e));
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				LOG.error(ExceptionUtils.getStackTrace(e));
			} catch (Exception e) {
				LOG.error(ExceptionUtils.getStackTrace(e));
			}
		}
	}
}
