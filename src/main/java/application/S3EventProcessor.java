package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.CollectionUtils;
import util.ThreadPoolUtils;

import au.com.bytecode.opencsv.CSVReader;

public class S3EventProcessor {
	private static Connection con = null;
	private static DynamoDB dynamoDB;
	
	private static final String TABLE_NAME = "notification";
	private final static Logger LOG = LoggerFactory.getLogger(S3EventProcessor.class);
	
	static AmazonS3 s3Client = new AmazonS3Client();
	static AmazonDynamoDBClient DDBClient = new AmazonDynamoDBClient();
	
	
	private static final char CSV_SEPERATOR 	= '|';
	private static final int LEN_OF_ELEMENT 	= 53;
	
	static {
		s3Client.setRegion(Region.getRegion(Regions.AP_NORTHEAST_1));
		DDBClient.setRegion(Region.getRegion(Regions.AP_NORTHEAST_1));
		
		try {
			con = getConnection();
		    con.setAutoCommit(false);
		    
		    LOG.info("Connection success ....");
	    } catch (SQLException | ClassNotFoundException e) {
	    	LOG.error(ExceptionUtils.getStackTrace(e));
			
			try {
				if (con != null) {
					con.close();
				}

			} catch (Exception ex) {
				LOG.error(ExceptionUtils.getStackTrace(ex));
			}
	    }
		
		dynamoDB = getDynamoDB();
	}
	
	public String handleRequest(S3Event event, Context context) {
		LOG.info("Receiving S3Event Notification ..........");
		
		ThreadPoolUtils pool = ThreadPoolUtils.create();
		List<S3EventNotificationRecord> records = event.getRecords();
		
		if (CollectionUtils.isNullOrEmpty(records)) {
			LOG.warn("Emtpy S3 Event Records....");
			return "";
		}
		
//		try {
//			con = getConnection();
//		    con.setAutoCommit(false);
//		    
//		    LOG.info("Connection success ....");
//	    } catch (SQLException | ClassNotFoundException e) {
//	    	LOG.error(ExceptionUtils.getStackTrace(e));
//			
//			try {
//				if (con != null) {
//					con.close();
//				}
//
//			} catch (Exception ex) {
//				LOG.error(ExceptionUtils.getStackTrace(ex));
//			}
//			return "";
//	    }
		
		if (con == null) {
			try {
				con = getConnection();
			    con.setAutoCommit(false);
			    
			    LOG.info("Connection success ....");
		    } catch (SQLException | ClassNotFoundException e) {
		    	LOG.error(ExceptionUtils.getStackTrace(e));
				
				try {
					if (con != null) {
						con.close();
					}

				} catch (Exception ex) {
					LOG.error(ExceptionUtils.getStackTrace(ex));
				}
		    }
			
			if (con == null) {
				LOG.info("Found no Connection");
				return null;
			}
		}
		
		if (dynamoDB == null) {
			dynamoDB = getDynamoDB();
		}
		
		Table table = dynamoDB.getTable(TABLE_NAME);
		
		if (table == null) {
			System.out.println("Found no table");
			return "";
		}
		
		CSVReader csvReader = null;
		String[] nextLine;
		
		int numOfLine = 0;
		
		LOG.info("Ready for getting warning message ....");
		
		for (S3EventNotificationRecord record : records) {
			
			S3Object object = getS3Object(record);
			
			LOG.info("type: {}", object.getObjectMetadata().getContentType());
			LOG.info("len: {}", object.getObjectMetadata().getContentLength());
			
			try {
				// 取得該object的各筆資料，交由Processor去解析和比對OpenData的資料
				csvReader = getCSVReader(object);

				while ((nextLine = csvReader.readNext()) != null) {	
					
					if (numOfLine > 0) {
						String[] element = nextLine.clone(); //Arrays.copyOf(nextLine, nextLine.length);
						
						if (element == null || element.length < LEN_OF_ELEMENT) {
							LOG.info("Invalid element ....");
							continue;
						}
//						pool.addTask(new Processor(element, con, table));
						pool.addTask(new MainProcessor(element, con, table));
					}
					numOfLine++;
				}
	
			} catch (Exception e) {
				LOG.error(ExceptionUtils.getStackTrace(e));
				continue;
			}
		}
		
		pool.close();
		while(!pool.awaitTermination());
		
//		try {
//			if (con != null) {
//				con.close();
//			}
//		} catch (Exception e) {
//			LOG.error(ExceptionUtils.getStackTrace(e));
//		}
		
		if (csvReader != null) {
			try {
				csvReader.close();
			} catch (Exception e) {
				LOG.error(ExceptionUtils.getStackTrace(e));
			}
		}
		
		LOG.info("Mission complete .... ");
		return null;
	}

	/**
	 * get DB connection
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	private static Connection getConnection() throws SQLException, ClassNotFoundException {
		
		String dbUrl = System.getenv("db_url");
		String dbUserName = System.getenv("db_username");
		String dbPassword = System.getenv("db_password");
		
		LOG.info("dbUrl: {}", dbUrl);
		LOG.info("dbUserName: {}", dbUserName);
		LOG.info("dbPassword: {}", dbPassword);
		
		Class.forName("org.postgresql.Driver");
    	LOG.info("Ready to make connection ...");
    	return DriverManager.getConnection(dbUrl, dbUserName, dbPassword);  
	}
	
	private static DynamoDB getDynamoDB() {
		return new DynamoDB(DDBClient);
	}
	
	/**
	 * 取得S3Object
	 * @param record
	 * @return
	 */
	private S3Object getS3Object(S3EventNotificationRecord record) {
		String srcBucket = record.getS3().getBucket().getName();
		String srcKey = record.getS3().getObject().getKey().replace('+', ' ');
		
		LOG.info("key: {}", srcKey);
		
		return s3Client.getObject(new GetObjectRequest(srcBucket, srcKey));
	}
	
	/**
	 * 奴得CSVReader object
	 * @param object
	 * @return
	 * @throws IOException
	 * @throws NullPointerException
	 */
	private CSVReader getCSVReader(S3Object object) throws IOException, NullPointerException {
		if (object == null) {
			throw new NullPointerException("S3Object is null");
		}
	
		LOG.info("Getting CSVReader ....");
		
		GZIPInputStream gis = new GZIPInputStream(object.getObjectContent());
		InputStreamReader isr = new InputStreamReader(gis);
		BufferedReader br = new BufferedReader(isr);
		
		return new CSVReader(br, CSV_SEPERATOR);
	}
}
