package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.document.Table;
import util.DBQueryUtils;
import util.FormulaUtils;
import util.OpenData;
import util.WarningStatus;

import Entity.WarningReq;
import repository.NotificationWarningRep;

public class SpeedMeasuringNotification extends NotificationWarningRep {
	private Connection con;
	private static final String LON = "lon";
	private static final String LAT = "lat";
	
	private static final String NORTH = "N";
    private static final String EAST = "E";
    private static final String SOUTH = "S";
    private static final String WEST = "W";
    private static final String NORTH_SOUTH = "NS";
    private static final String WEST_EAST = "WE";
    
    private static final String DIRECT = "direct";
    
    private static final Set<String> DIRECTION_SET = new HashSet<>(Arrays.asList(NORTH, EAST, SOUTH, WEST, NORTH_SOUTH, WEST_EAST));
	
	private final static Logger LOG = LoggerFactory.getLogger(SpeedMeasuringNotification.class);
	
	public SpeedMeasuringNotification(Connection con, Table table) {
		super(con, table);
		this.con = con;
	}
	
	
	/**
	 * 從rawData(WarningReq)得到的資料如經緯度和速度等比對openData資料，查看是否有達到推播的條件(warning)
	 * @param openData
	 * @param req
	 */
	public void checkWarning(WarningReq req) {
		// checking request 
		if (req == null || req.getUserId() == null || req.getLat() == null || req.getLon() == null || req.getLat() == 0 || req.getLon() == 0 || req.getHash() == null) {
			LOG.warn("[WarningReq]: Invalid request");
			return;
		}
		
		ResultSet rs = null;
		Double lat, lon;	// OpenData 的經緯度
		
		// 取得該經緯度附近OpenData的資料
		try {
			LOG.info("[WarningReq]: Getting OpenData....");
			rs = DBQueryUtils.getOpenDataInsideRangeQuery(OpenData.SPEED_CAMERA, req, con);
		} catch (SQLException e) {
			LOG.error(e.getMessage());
			LOG.error(ExceptionUtils.getStackTrace(e));
			return;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			LOG.info("Getting Data Error: " + e.getMessage());
		}
		
		if (rs == null) {
			LOG.warn("[WarningReq]: ResultSet from openData is null");
			return;
		}
		
		try {
			while (rs.next()) {
				
				String dir = rs.getString(DIRECT);	
				// 目前以車子方向分成4個象限，判斷測速照向方向是否和車輛行徑方向大約一致
				if (DIRECTION_SET.contains(dir) && !isAlignDirection(req.getQuadrant(), dir)) {
					LOG.info("[OpenData]: Speed_measuring is not in your direction");
					continue;
				}
						
				lon = rs.getDouble(LON);
				lat = rs.getDouble(LAT);
				
				LOG.info("[OpenData]: OpenData src latlng1 : (" + lat + ", " + lon + ")");
				
				WarningStatus warningStatus = FormulaUtils.evaluateWarningStatus(req, lat, lon);
			
				LOG.info("[WarningStatus]: {}", warningStatus);
				
				// 超過範圍不推播
				if (WarningStatus.NO_WARNING.equals(warningStatus)) {
					LOG.info("[OpenData]: According to this openData set: It's not in the critical range");
					continue;
				} 
			
				// 推到推播條件，將資料計錄到DB
				if (WarningStatus.CRITICAL.equals(warningStatus)) {
					preparePushNotification(req.getUserId(), req.getLat(), req.getLon(), OpenData.SPEED_CAMERA, rs, lat, lon, req.getEventTime());
					break;
				}
		    }
			
		} catch (Exception e) {
			LOG.error(ExceptionUtils.getStackTrace(e));
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				LOG.error(ExceptionUtils.getStackTrace(e));
			} catch (Exception e) {
				LOG.error(ExceptionUtils.getStackTrace(e));
			}
		}
	}
	
	/**
	 * 查看角度是和direction描述大致穩和，方向區分為 N, E, S, W
	 * @param bearing
	 * @param dir
	 * @return
	 */
	private boolean isAlignDirection(int quadrant, String dir) {
		
		if (NORTH_SOUTH.equals(dir) || WEST_EAST.equals(dir)) {
			return true;
		}
		
		if (quadrant == 1) {
			return NORTH.equals(dir) || EAST.equals(dir);
		// 東南向
		} else if (quadrant == 4) {
			return SOUTH.equals(dir) || EAST.equals(dir);
		// 西北向
		} else if (quadrant == 2) {
			return NORTH.equals(dir) || WEST.equals(dir);
		// 西南向
		} else if (quadrant == 3) {
			return SOUTH.equals(dir) || WEST.equals(dir);
		} 
		
		return false;
	}
}
