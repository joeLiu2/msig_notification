package Entity;

import java.sql.Timestamp;

import ch.hsr.geohash.GeoHash;

public class WarningReq {
	private String userId;
	private Double lon;
	private Double lat;
	private GeoHash hash;
	private Timestamp eventTime;
	private Double bearing;
	private int quadrant;
	private Double speed;
	private Double bearingRange;
	private double[] distRange;
	
	public String getUserId() {
		return userId;
	}
	public Double getLon() {
		return lon;
	}
	public Double getLat() {
		return lat;
	}
	public Timestamp getEventTime() {
		return eventTime;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public void setEventTime(Timestamp eventTime) {
		this.eventTime = eventTime;
	}
	public Double getBearing() {
		return bearing;
	}
	public void setBearing(Double bearing) {
		this.bearing = bearing;
	}
	public Double getSpeed() {
		return speed;
	}
	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	public GeoHash getHash() {
		return hash;
	}
	public void setHash(GeoHash hash) {
		this.hash = hash;
	}
	public int getQuadrant() {
		return quadrant;
	}
	public void setQuadrant(int quadrant) {
		this.quadrant = quadrant;
	}
	public Double getBearingRange() {
		return bearingRange;
	}
	public double[] getDistRange() {
		return distRange;
	}
	public void setBearingRange(Double bearingRange) {
		this.bearingRange = bearingRange;
	}
	public void setDistRange(double[] distRange) {
		this.distRange = distRange;
	}
}
