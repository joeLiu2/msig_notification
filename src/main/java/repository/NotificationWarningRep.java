package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import util.DBQueryUtils;
import util.MessageUtils;
import util.OpenData;

import Entity.WarningReq;

abstract public class NotificationWarningRep {
	
	private final static Logger LOG = LoggerFactory.getLogger(NotificationWarningRep.class);
	
	private Connection con;
	private Table table;
//	private Map<String, String> tokenMap;
	
	private static final String USER_ID 		= "user_id";
	private static final String NOTI_ID 		= "noti_id";
	private static final String SOURCE_LON 		= "source_lon";
	private static final String SOURCE_LAT 		= "source_lat";
	private static final String EVENT_LON		= "event_lon";
	private static final String EVENT_LAT 		= "event_lat";
	private static final String EVENT_TIME 		= "event_time";
	private static final String FCM_TOKEN		= "fcm_token";
	private static final String TIME_STAMP		= "time_stamp";
	private static final String CONTENT			= "content";
	private static final String TITLE			= "title";
	private static final String TYPE			= "type";
	private static final String TOKEN			= "token";
	
	private static final int TEN_MIN_IN_MILLI = 10 * 60 * 1000;
	
	public NotificationWarningRep(Connection con, Table table) {
		this.con = con;
		this.table = table;
//		this.tokenMap = tokenMap;
	}
	
	abstract public void checkWarning(WarningReq req);
	
	/**
	 * insert notification to table
	 * @param userId
	 * @param eventLat
	 * @param eventLon
	 * @param openData
	 * @param rs
	 * @param sourceLat
	 * @param sourceLon
	 * @param sound
	 * @param eventTime
	 * @param rainType
	 */
	public void insertNotificationToDB(String userId, double eventLat, double eventLon, OpenData openData, 
			Double sourceLat, Double sourceLon, Timestamp eventTime, String warningMsg) {
	    String notiId = MessageUtils.getUniqId();
	    
	    String token;
//	    if (!tokenMap.containsKey(userId)) {
//    		token = getFcmToken(userId);
//    		if (token != null) {
//    			tokenMap.put(userId, token);
//    		}
//    	}
    		
//    	token = tokenMap.get(userId);
	    token = getFcmToken(userId);	
    	if (StringUtils.isBlank(token)) {
    		LOG.warn("Token is blank...");
    		return;
    	}
    	
    	LOG.info("Inserting nortification record to DynamoDB...");
    	try {
    		insertNotification2DynamoDB(userId, MessageUtils.getTitle(openData), notiId, warningMsg, token, openData,
    				sourceLon, sourceLat, eventLon, eventLat, eventTime);
    	} catch (Exception e) {
    		LOG.info("Exception:{}", e.getMessage());
    		LOG.error(ExceptionUtils.getStackTrace(e));
    	}
	}
	
	/**
	 * 將進料insert進DynamoDB, 做後續推播
	 * @param userId
	 * @param title
	 * @param notiId
	 * @param currTime
	 * @param warningMsg
	 * @param token
	 * @param sound
	 * @throws Exception
	 */
	public void insertNotification2DynamoDB(String userId, String title, String notiId, String warningMsg, String token, 
			OpenData openData, double sourceLon, double sourceLat, double eventLon, double eventLat, Timestamp eventTime) throws Exception {
		// corner case check
		if (userId == null || title == null || notiId == null || warningMsg == null || token == null || openData == null || eventTime == null) {
			LOG.info("userId: {}", userId);
			LOG.info("title: {}", title);
			LOG.info("notiId: {}", notiId);
			LOG.info("warningMsg: {}", warningMsg);
			LOG.info("token: {}", token);
			LOG.info("openData: {}", openData.toString());
			LOG.info("eventTime: {}", eventTime.toString());
			
			LOG.warn("push infomation is not complete");
			return;
		}
		
//		setPusContentTimer(userId, warningMsg, 60 * 5);
		
		long currTimeStamp = System.currentTimeMillis();
		// insert notification to dynamoDB
		PutItemOutcome outcome = table
        		.putItem(new Item().withPrimaryKey(USER_ID, userId, NOTI_ID, notiId)
        		.withLong(TIME_STAMP, currTimeStamp)
        		.withString(CONTENT, warningMsg)
        		.withString(TITLE, title)
        		.withString(TYPE, openData.toString())
        		.withString(TOKEN, token)
				.withDouble(SOURCE_LON, sourceLon)
				.withDouble(SOURCE_LAT, sourceLat)
				.withDouble(EVENT_LON, eventLon)
				.withDouble(EVENT_LAT, eventLat)
				.withLong(EVENT_TIME, eventTime.getTime()));

	    LOG.info(outcome.toString());
	}
	
	/**
	 * get WarningMsg according to OpenData type and verify if the same message is inserted to DB within 10 mins, 
	 * insert push notification to DB
	 * @param userId
	 * @param eventLat
	 * @param eventLon
	 * @param openData
	 * @param rs
	 * @param sourceLat
	 * @param sourceLon
	 * @param sound
	 * @param eventTime
	 * @param rainType
	 */
	protected void preparePushNotification(String userId, double eventLat, double eventLon, OpenData openData, ResultSet rs, Double sourceLat, Double sourceLon, Timestamp eventTime) {
		String warningMsg = null;
		
		try {
	    	warningMsg = MessageUtils.createWarningMsg(openData, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	    
	    if (StringUtils.isBlank(warningMsg)) {
	    	System.out.println("No WARNING MESSAGE found or no warning at all");
	    	return;
	    }
	    
	    // 上一筆同樣的推播訊息在10分鐘故不進行推播
	    if (isHappenWithInTime(userId, warningMsg)) {
	    	LOG.info("UserID:" + userId + " checking the pushNotification - Within 10 min");
	    	return;
	    }
	    
	    insertNotificationToDB(userId, eventLat, eventLon, openData, sourceLat, sourceLon, eventTime, warningMsg);
	}

	/**
	 * 判斷發送時間是否在10分鐘內
	 * @param userId
	 * @param pushContents
	 * @return
	 */
	private boolean isHappenWithInTime(String userId, String pushContents) {
		System.out.println("Checking timer...");

//		RedisClient redisClient = RedisClient.create("redis://d9KSdCdxoFdEVQZkJ0k9h3xEW4Lc1n7q@redis-18423.c54.ap-northeast-1-2.ec2.cloud.redislabs.com:18423");
//		StatefulRedisConnection<String, String> connection = redisClient.connect();
//		RedisCommands<String, String> jedis = connection.sync();
//		
//		boolean isWithinTime = true;
//		
//		String key = MessageUtils.getPushContentKey(userId, pushContents);
//		System.out.println("key:" + key);
//		isWithinTime = jedis.exists(key) > 0;
//		
//		connection.close();
//		redisClient.shutdown();
//		
//		return isWithinTime;
		
		long currTimeStamp = System.currentTimeMillis();
		
		
        Map<String, Object> valueMap = new HashMap<>();
        valueMap.put(":id", userId);
        valueMap.put(":content", pushContents);
        valueMap.put(":time", currTimeStamp - TEN_MIN_IN_MILLI); // 確認是否在10分鐘內
        
        QuerySpec querySpec = new QuerySpec()
        		.withProjectionExpression("noti_id")
        		.withKeyConditionExpression("user_id = :id")
        		.withFilterExpression("content = :content and time_stamp >= :time")
                .withValueMap(valueMap);
        

        ItemCollection<QueryOutcome> items = null;
        Iterator<Item> iterator = null;

        try {
            items = table.query(querySpec);
            
            if (items == null) {
            	return false;
            }
            
            iterator = items.iterator();
            
            if (iterator != null && iterator.hasNext()) {
            	return true;
            }
            
            return false;
        } catch (Exception e) {
        	LOG.info(ExceptionUtils.getStackTrace(e));
        	LOG.error("Failed confirming timestamp for the same event in dynamoDB and same user");
        	return true;
        }
	}
	
	/**
	 * getting FCM (device_id)token for push notification
	 * @param userId user_id
	 * @return device_id (token)
	 */
	private String getFcmToken(String userId) {
		
		try {
			ResultSet rs = DBQueryUtils.getDeviceId(con, userId);
			
			if (rs != null && rs.next()) {
				String token = rs.getString(FCM_TOKEN);
				return token;
			}
		} catch (SQLException e) {
			LOG.info(ExceptionUtils.getStackTrace(e));
			LOG.error("failed getting fcmToken...");
		} catch (Exception e) {
			LOG.info(ExceptionUtils.getStackTrace(e));
			LOG.error("failed from getting fcmToken...");
		}
		
		return null;
	}
}
